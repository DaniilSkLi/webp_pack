use image::io::Reader as ImageReader;
use image::{DynamicImage, EncodableLayout};
use webp::{Encoder, WebPMemory};

use std::fs::File;
use std::io::Write;

fn main() {
    let image: DynamicImage = ImageReader::open("test.png")
        .expect("Image don't found.")
        .decode()
        .expect("Image decode error.");

    let encoder: Encoder = Encoder::from_image(&image).unwrap();
    let encoded_webp: WebPMemory = encoder.encode(65f32);

    let mut webp_image = File::create("output.webp").unwrap();
    webp_image.write_all(encoded_webp.as_bytes()).unwrap();
}
